/*
 * roatry_encoder_motor.cpp
 *
 * Created: 7/26/2016 2:21:52 PM
 * Author : David Michelman, daweim0@gmail.com
 */ 

#define F_CPU 8000000UL // 16 MHz clock speed
#define bit_get(p,m) ((p) & (1 << m))
#define bit_set(p,m) ((p) |= (1 << m))
#define bit_clear(p,m) ((p) &= ~(1 << m))


#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <stdio.h>

#include "uart.h"

volatile int32_t steps = 0;
volatile char pins;

int usart_putchar_printf(char var, FILE *stream);

static FILE mystdout = FDEV_SETUP_STREAM(usart_putchar_printf, NULL, _FDEV_SETUP_WRITE);

int main(void)
{
	OSCCAL = 0x92;  // Calibrate internal oscillator for 8 mhz
	
	EICRA |= (1 << ISC00) | (1 << ISC10);
	EIMSK |= (1 << INT0) | (1 << INT1);
	DDRD = 0x00;
	
	uart_init(UART_BAUD_SELECT(250000, F_CPU));
	
	stdout = &mystdout;
	
	
	sei();
	
	DDRB = 0xFF; 
	while(1) 
	{
		_delay_ms(10000); //1 second delay
		if(PORTD == PORTB) {
			_delay_ms(0);
		}
	}
}


int usart_putchar_printf(char var, FILE *stream) {
	if (var == '\n') uart_putc('\r');
	uart_putc(var);
	return 0;
}


ISR (INT0_vect, ISR_NOBLOCK) {
	pins = PIND;
	PORTB = 0xff;
	_delay_ms(100);
	PORTB = 0x00;
	if(bit_get(pins, PORTD2)) {
		 // pin 3 is high
		 if(bit_get(pins, PORTD3)) {
			 // pin 4 is high
			 steps -= 1;
		 }
		 else {
			 // pin 4 is low
			 steps += 1;
		 }
	}
	else {
		// pin 3 is low
		if(bit_get(pins, PORTD3)) {
			// pin 4 is high
			steps += 1;
		}
		else {
			// pin 4 is low
			steps -= 1;
		}
	}

}


ISR (INT1_vect) {
	if(PORTD & (1 << PORTD3)) {
		// pin 4 is high
		PORTB |= 1 << PORTB0;
	}
	else {
		// pin 4 is low
		PORTB = 0x00;
	}
}