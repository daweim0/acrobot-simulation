Required software packages
https://www.python.org/download/releases/2.6/ (32 bit)
http://pygame.org/ftp/pygame-1.9.1.win32-py2.6.msi
http://www.scipy.org/
http://pyode.sourceforge.net/
http://www.lfd.uci.edu/~gohlke/pythonlibs/#ode (might no longer work)

Contact David Michelman (daweim0@gmail.com) with any questions.